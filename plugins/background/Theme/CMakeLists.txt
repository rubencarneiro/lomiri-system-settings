add_library(LomiriThemeSettings MODULE
plugin.cpp theme.cpp plugin.h theme.h)

target_link_libraries(LomiriThemeSettings Qt5::Qml Qt5::Quick)

set(PLUG_DIR ${PLUGIN_PRIVATE_MODULE_DIR}/Lomiri/SystemSettings/Background/Theme)
install(TARGETS LomiriThemeSettings DESTINATION ${PLUG_DIR})
install(FILES qmldir DESTINATION ${PLUG_DIR})
