/*
 * Copyright (C) 2023 UBports Foundation
 * 
 * Authors:
 *    Muhammad <muhammad23012009@hotmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QObject>
#include <QSettings>
#include <QStandardPaths>
#include <QDebug>

#define LIGHT_THEME "Lomiri.Components.Themes.Ambiance"
#define DARK_THEME "Lomiri.Components.Themes.SuruDark"

class Theme : public QObject
{
  Q_OBJECT
  Q_PROPERTY(bool darkModeEnabled READ darkModeEnabled WRITE setDarkModeEnabled NOTIFY darkModeChanged)

public:
  explicit Theme(QObject *parent = 0);
  ~Theme();

  bool darkModeEnabled();
  void setDarkModeEnabled(bool enabled);

  void enableLightMode();
  void enableDarkMode();

Q_SIGNALS:
  void darkModeChanged();

private:
  QSettings m_settings;
};
